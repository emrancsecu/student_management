@extends('master')
@section('content')

<table class="table table-bordered table-striped table-dark">
  <tr>
    <th>SL</th>
    <th>Name</th>
    <th>Email</th>
    <th>Registration</th>
    <th>Department</th>
    <th>info</th>
    <th>Action</th>
  </tr>
  @php $i=0; @endphp

  @foreach ($students as $student)

  @php $i++; @endphp
  <tr>
    <td>{{$i}}</td>
    <td>{{$student->name}}</td>
    <td>{{$student->email}}</td>
    <td>{{$student->registration_id}}</td>
    <td>{{$student->department_name}}</td>
    <td>{{$student->info}}</td>
    <td>
      <a href="{{route('edit',$student->id)}}" class="btn btn-primary" style="float:left;margin-right:5px;">Edit</a>
      <form action="{{route('delete',$student->id)}}" method="post" class="form-inline">
        {{csrf_field()}}
         <input type="submit" name="submit" value="delete" class="form-control btn btn-danger" >
      </form>
    </td>
  </tr>
  @endforeach
</table>


@endsection

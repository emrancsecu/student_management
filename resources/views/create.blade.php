@extends('master')

@section('content')


  <h2>Add New Student</h2>
  <form action="{{route('store')}}" method="post">
    {{csrf_field()}}

    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter Your Name" name="name">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="registration">Registration:</label>
      <input type="number" class="form-control" id="registration_id" placeholder="Enter Your Registration Number" name="registration_id">
    </div>
    <div class="form-group">
      <label for="department">Department:</label>
      <input type="text" class="form-control" id="department_name" placeholder="Enter Your Department Name" name="department_name">
    </div>
    <div class="form-group">
      <label for="info">Info:</label>
       <textarea class="form-control" rows="5" id="info" name="info" placeholder="Write about you"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection
